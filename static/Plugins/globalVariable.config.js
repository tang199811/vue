define([
    'require',
], function(require, factory) {
    'use strict';
    //项目后台接口配置
    let ServerBase="http://192.168.75.28:9090/";//后台服务接口
    var Configuration = {
        login:ServerBase+"user/login",//登陆
        findAllUser:ServerBase+"user/findAllUser",//获取用户集合数据
        findUserByUserName:ServerBase+"user/findUserByUserName",//根据用户名查询
        findAllDept:ServerBase+"dept/findAllDept",//获取部门集合数据
        addUser:ServerBase+"user/addUser",//添加用户接口
        deleteUserById:ServerBase+"user/deleteUserById",//根据id删除
       findAllStduent:ServerBase+"stduent/findAllStudent",//查询所有学生
       getMyAddress:ServerBase+"address/getMyAddress",//查询所有收货地址
       addAddress:ServerBase+"address/addAddress",//添加收货地址
       updateAddress:ServerBase+"address/updateAddress",//修改地址信息
       updateDefultAddress:ServerBase+"address/updateDefultAddress",//修改默认地址
       deleteAddress:ServerBase+"address/deleteAddress",//删除收货地址
    };
    return Configuration;
});