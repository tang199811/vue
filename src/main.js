import Vue from 'vue'
import App from './App'
import router from './router'
import iView from 'iview';   
import 'iview/dist/styles/iview.css'; 
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from 'element-ui';
import axios from 'axios'
import apiServer from './../static/Plugins/globalVariable.config';
import store from './store/index.js'   //引入 vuex
import less from 'less'
Vue.use(less)
Vue.use(iView);  
Vue.use(ElementUI);
Vue.config.productionTip = false;
Vue.prototype.$axios=axios;
Vue.prototype.$apiServer=apiServer; 
// 登陆校验//未登录不可进入
router.beforeEach(function (to,from,next) {
    if(to.meta.needLogin){
        if(localStorage.getItem("token")){
              next()
        }else{
          Vue.prototype.$message({
            type: "warning",
            message: "请登录!",
          });
            next({name:"Login"});
        }
    }else{
      next();
    }
})
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
