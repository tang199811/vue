import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/page/Login'
import Home from '@/components/page/Home';
import UserAdd from '@/components/page/userPage/UserAdd';
import UserHome from '@/components/page/user/UserHome'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: HelloWorld
    },{
      path:"/",
      name:"Login",
      component:Login
    },
    {
      path:"/Home",
      name:"Home",
      component:Home,
      meta:{needLogin:true}//需要登陆验证
    },

  ]
})
