import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export default new Vuex.Store({
    state:{
        //存储数据
        isShowAddUser:false,
        isShowFindUser:true,
        user:{},
    },
    mutations:{
        //数据处理方法
        UserHome(state){
            
        }
    },
    getters:{
        //数据包装
    }
})